# MyBinder Environment

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/rouault-team-public%2Fteaching%2Fbinder-env/HEAD)

This repository allows separating the jupyter environment and content,
following this discussion:
[Discourse discussion](https://discourse.jupyter.org/t/tip-speed-up-binder-launches-by-pulling-github-content-in-a-binder-link-with-nbgitpuller/922)
