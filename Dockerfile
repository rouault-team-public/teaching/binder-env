FROM python:3.10-slim

# install the notebook package
RUN pip install --no-cache --upgrade pip && \
    pip install --no-cache pipenv

### create user with a home directory
ARG NB_USER=jovyan
ARG NB_UID=1000
ENV NB_USER ${NB_USER}
ENV NB_UID ${NB_UID}
ENV HOME /home/${NB_USER}

RUN adduser --disabled-password \
    --gecos "Default user" \
    --uid ${NB_UID} \
    ${NB_USER}

# Make sure the contents of our repo are in ${HOME}
COPY . ${HOME}
USER root
RUN chown -R ${NB_UID} ${HOME}
USER ${NB_USER}
WORKDIR ${HOME}

RUN cd ${HOME} 
RUN pipenv install

# Run Jupyter Lab via pipenv in conda environment
EXPOSE 8888
RUN echo '#!/bin/bash -i\npipenv run "$@"' > .entrypoint.sh && \
    chmod +x .entrypoint.sh

ENTRYPOINT ["./.entrypoint.sh"]
CMD ["jupyter", "lab", "--ip", "0.0.0.0"]
